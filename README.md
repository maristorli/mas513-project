# mas513 project

This repositiry contains the files used for the project: Radar detection of cars in parking lot from above

- In the **Experiment folder** all files used for the experiement that was performed during this project are included. This includes a configuration file for the radar, a customized .launch file, the recorded data saved in a .csv file, the python script that was used to detect the car and a figure of the experimental system.
- In the **Videos folder**, two videos showing the results are uploaded.
