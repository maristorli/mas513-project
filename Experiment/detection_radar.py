# IMPORTNIG DATA FROM .CSV AND PERFORMING CLUSTERING TO DETECT OBJECTS ON RADAR POINT CLOUD
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

fields = ['.x', '.y']
data_np = []

data = pd.read_csv('car_7m.csv', skipinitialspace=True, usecols=fields)
data_np = data.to_numpy()   # covert data from csv from daataframe to numpy array


# filter out noise close to radar: remove data points closer than 2m  (in x)
data_np_rem =[]
min_distance = 2.0
n_dp = len(data_np)
for k in range(n_dp):
    if data_np[k,0] > min_distance:
        data_np_rem.append(data_np[k,:]) # returns a list of arrays
data_np_rem = np.vstack(data_np_rem)

# plot all data points
plt.scatter(data_np[:,1], data_np[:,0], color='gray')
plt.scatter(data_np_rem[:,1], data_np_rem[:,0])
plt.title('Point cloud from radar, filtered')
plt.xlabel('horizontal distance [m]')
plt.ylabel('depth; distance away from radar [m]')
plt.show()

# Clustering
n = 5
num_points_lim = 280
kmeans =KMeans(n_clusters=n)    # initialize the class object
cluster = kmeans.fit_predict(data_np_rem)  # predict the labels of clusters; returns the array of cluster labels each data point belongs to
centerpoints = kmeans.cluster_centers_


points_in_cluster = []
centerpoints_lim = []
clusters_accepted = 0
for i in range(n):
    points_in_cluster.append(data_np_rem[cluster == i])
    
    # plot all points in each cluster
    plt.scatter(points_in_cluster[i][:,1], points_in_cluster[i][:,0], label = i)    # x and y are defined in rospackage as:                     
                                                                                    # x: distance of object front from radar (away)
                                                                                    # y: distance of object right/left from the radar (horizontally)
    # Remove all center points that belong to a cluster with too few data points
    if len(points_in_cluster[i]) > num_points_lim:
        centerpoints_lim.append(centerpoints[i,:])
        print('object at:')
        print(i)
c = len(centerpoints_lim)
cen = np.stack(centerpoints_lim)
 
for j in range(c):
    plt.scatter(cen[j,1], cen[j,0], color='black', marker="p")      # plot estimated center of each accepted object
plt.title('Clustered point cloud from radar')
plt.xlabel('horizontal distance [m]')
plt.ylabel('depth/distance out of radar [m]')    
plt.legend()                                                                      
plt.show()
